# Ansible Role: Oh My Zsh

[![pipeline status](https://gitlab.com/esme5/td_ansible/badges/master/pipeline.svg)](https://gitlab.com/lcaminale/ansible-role-oh-my-zsh/-/commits/master)

Role to download, install and configure [Oh-My-Zsh](http://ohmyz.sh/).

Requirements
------------

* Ansible >= 2.10.1

* Linux Distribution

    * Debian Family

        * Ubuntu

            * Bionic (18.04)


Role Variables
--------------

Go to edit :  [vars](vars/main.yml) 

```yaml
user_name: lolo
config_zsh_profile:
  oh_my_zsh_version: "a5e706d749e4218820391cb7ea374c6ddd248933" # COMMIT 
  zsh_syntax_highlighting_version: 0.7.1
  theme: robbyrussell  # https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
  plugins:
    - autojump
    - git
    - zsh-syntax-highlighting
``` 

Example Playbook
----------------

How to use this role 

```yaml
- hosts: servers
  roles:
     - { role: setup_ubuntu }
```

Development & Testing
---------------------

This project uses [Molecule](http://molecule.readthedocs.io/) to aid in the
development and testing; the role is unit tested using
[Testinfra](http://testinfra.readthedocs.io/) and
[pytest](http://docs.pytest.org/).

To develop or test you'll need to have installed the following:

* [Docker](https://www.docker.com/)
* [Python](https://www.python.org/) (including python-pip)

For this one, you can use pip : 

* [Ansible](https://www.ansible.com/)
* [Molecule](http://molecule.readthedocs.io/)

```shell script
pip install -r requirements
```

All dependancies are in [requirements](requirements.txt)🤓

